package main

import (
	"gitlab.yoshino-s.xyz/yoshino-s/cilium-envoy-hook/cmd"
)

func main() {
	cmd.Execute()
}
