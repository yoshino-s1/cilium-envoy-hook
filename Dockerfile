FROM golang:1.20-alpine as build-stage

ARG VERSION
ENV VERSION=${VERSION}

WORKDIR /src
COPY . .
RUN go env -w GOPROXY=https://goproxy.cn,direct
RUN CGO_ENABLED=0 go build -o /hook --ldflags "-w -extldflags '-static' -X cmd.Version=${VERSION:-dev}" .

# Final image.
FROM alpine:latest
COPY --from=build-stage /hook /usr/local/bin/hook
ENTRYPOINT ["/usr/local/bin/hook"]