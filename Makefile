
SHELL := $(shell which bash)
OSTYPE := $(shell uname)
DOCKER := $(shell command -v docker)
GID := $(shell id -g)
UID := $(shell id -u)
VERSION ?= $(shell git describe --tags --always)

UNIT_TEST_CMD := ./scripts/check/unit-test.sh
INTEGRATION_TEST_CMD := ./scripts/check/integration-test.sh
CHECK_CMD := ./scripts/check/check.sh
DEP_CMD := ./scripts/deps.sh
GEN_CERTS_CMD := ./scripts/gen-certs.sh


help: ## Show this help
	@echo "Help"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "    \033[36m%-20s\033[93m %s\n", $$1, $$2}'
	@echo -e "\033[0m"

.PHONY: default
default: help

.PHONY: deps
deps:  ## Fixes the dependencies
	./scripts/deps.sh

.PHONY: unit-test
unit-test:  ## Runs unit test in CI environment (without docker).
	@$(UNIT_TEST_CMD)

.PHONY: check
check:  ## Runs checks in CI environment (without docker).
	@$(CHECK_CMD)

.PHONY: integration-test
integration-test: ## Runs integraton test in CI environment (without docker).
	@$(INTEGRATION_TEST_CMD)

.PHONY: gen-deploy-certs
gen-deploy-certs: ## Generate deploy files webhook certificates.
	@$(GEN_CERTS_CMD)
