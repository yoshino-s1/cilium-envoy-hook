package cmd

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	_ "embed"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"go.uber.org/zap"
)

//go:embed deploy.yaml
var template string

var deoplyCmd = &cobra.Command{
	Use: "deploy",
	Run: func(cmd *cobra.Command, args []string) {
		// 1. create ca and key for webhook
		key, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			zap.L().Fatal("failed to generate key", zap.Error(err))
		}
		cfg := &x509.Certificate{
			SerialNumber:          big.NewInt(1),
			Subject:               pkix.Name{CommonName: fmt.Sprintf("%s.%s.svc", app, namespace)},
			NotBefore:             time.Now(),
			NotAfter:              time.Now().Add(365 * 24 * time.Hour),
			BasicConstraintsValid: true,
			IsCA:                  true,
			KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
			DNSNames:              []string{fmt.Sprintf("%s.%s.svc", app, namespace)},
		}
		cert, err := x509.CreateCertificate(rand.Reader, cfg, cfg, &key.PublicKey, key)
		if err != nil {
			zap.L().Fatal("failed to create cert", zap.Error(err))
		}

		template = strings.ReplaceAll(template, "NAMESPACE", namespace)
		template = strings.ReplaceAll(template, "APP", app)
		template = strings.ReplaceAll(template, "CA_BUNDLE", base64.StdEncoding.EncodeToString(pem.EncodeToMemory(&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: cert,
		})))
		template = strings.ReplaceAll(template, "KEY_PEM", base64.StdEncoding.EncodeToString(pem.EncodeToMemory(&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(key),
		})))
		fmt.Println(template)
	},
}

var (
	namespace string
	app       string
)

func init() {
	rootCmd.AddCommand(deoplyCmd)

	deoplyCmd.PersistentFlags().StringVar(&namespace, "namespace", "default", "namespace")
	deoplyCmd.PersistentFlags().StringVar(&app, "app", "default", "app")
}
