package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	// Version is set at compile time.
	Version = "dev"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(Version)
	},
}
